#include <logging/log.h>
LOG_MODULE_REGISTER(net_dhcpv4_client_sample, LOG_LEVEL_DBG);

#include <zephyr.h>
#include <linker/sections.h>
#include <errno.h>
#include <stdio.h>

#include <net/net_if.h>
#include <net/net_core.h>
#include <net/net_context.h>
#include <net/net_mgmt.h>
#include <stdio.h>
#include <sys/sem.h>
#include <net/socket.h>
#include <net/dns_resolve.h>
#include <net/buf.h>

#define SERVER_ADDR "google.com"
#define SERVER_PORT "443"

K_SEM_DEFINE(my_sem, 0, 1)

static struct net_mgmt_event_callback mgmt_cb;

static void handler(struct net_mgmt_event_callback *cb,
		    uint32_t mgmt_event,
		    struct net_if *iface)
{
	int i = 0;

	if (mgmt_event != NET_EVENT_IPV4_ADDR_ADD) {
		return;
	}

	for (i = 0; i < NET_IF_MAX_IPV4_ADDR; i++) {
		char buf[NET_IPV4_ADDR_LEN];

		if (iface->config.ip.ipv4->unicast[i].addr_type !=
							NET_ADDR_DHCP) {
			continue;
		}

		LOG_INF("Your address: %s",
			log_strdup(net_addr_ntop(AF_INET,
			    &iface->config.ip.ipv4->unicast[i].address.in_addr,
						  buf, sizeof(buf))));
		LOG_INF("Lease time: %u seconds",
			 iface->config.dhcpv4.lease_time);
		LOG_INF("Subnet: %s",
			log_strdup(net_addr_ntop(AF_INET,
				       &iface->config.ip.ipv4->netmask,
				       buf, sizeof(buf))));
		LOG_INF("Router: %s",
			log_strdup(net_addr_ntop(AF_INET,
						 &iface->config.ip.ipv4->gw,
						 buf, sizeof(buf))));
	}
	k_sem_give(&my_sem);
}


void main(void)
{
	if(mdm_sim7080_start_network() == 0) {
		LOG_INF("Network interface started");
	}

	struct net_if *iface;

	LOG_INF("Run dhcpv4 client");

	net_mgmt_init_event_callback(&mgmt_cb, handler,
				     NET_EVENT_IPV4_ADDR_ADD);
	net_mgmt_add_event_callback(&mgmt_cb);

	iface = net_if_get_default();

	net_dhcpv4_start(iface);
	k_sem_take(&my_sem, K_FOREVER);

	while (1) {
	    k_sleep(K_MSEC(1000));

	    struct addrinfo *haddr;
	    struct addrinfo hints = {
		    .ai_family = AF_INET,
		    .ai_socktype = SOCK_STREAM,
	    };

	    int rc = -EINVAL;
	    rc = getaddrinfo("google.com", "443", &hints, &haddr);
	    if (rc == 0) {
		LOG_INF("DNS resolved");
	    }else {
		LOG_INF("Hostname is not resolved %s:%s, %d", log_strdup(SERVER_ADDR), SERVER_PORT, rc);
		continue;
	    }

	    LOG_INF("Got add info");
	    char addr[17] = { 0 };
	    snprintf(addr, sizeof(addr), "%u.%u.%u.%u",
			    net_sin((haddr)->ai_addr)->sin_addr.s4_addr[0],
			    net_sin((haddr)->ai_addr)->sin_addr.s4_addr[1],
			    net_sin((haddr)->ai_addr)->sin_addr.s4_addr[2],
			    net_sin((haddr)->ai_addr)->sin_addr.s4_addr[3]);
	    freeaddrinfo(haddr);
	    LOG_INF("Got IP %s", log_strdup(addr));
	}
}
